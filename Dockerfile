FROM python:3

WORKDIR /app/flask-pipeline

RUN pip install Flask

COPY . .

CMD ["python", "./main.py"]